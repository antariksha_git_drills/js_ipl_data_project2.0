const fs=require("fs");
const csv=require("csvtojson");
const promises=require("fs/promises")

async function csvtojson(fileName){
    const data=await fs.promises.readFile(`./src/data/${fileName}.csv`,'utf-8');
    const arr=await csv().fromString(data);
    await fs.promises.writeFile(`./src/data/${fileName}.json`,JSON.stringify(arr,null,2));
}
csvtojson('matches');

module.exports=csvtojson;