const fs=require("fs");
const promises=require("fs/promises")
const csvtojson=require("./jsonconvert.js")

const matchesPerYear=require("./src/server/1-MatchesPerYear.js")
const matchesWonPerTeamPerYear=require("./src/server/2-MatchesWonPerTeamPerYear.js")
const runsConcededPerTeam=require("./src/server/3-RunsConcedePerTeam.js")
const economy=require("./src/server/4-EconomicalBowlers.js")
const tossAndMatchWin=require("./src/server/5-TossAndMatchWin.js")
const playerOfMatch=require("./src/server/6-PlayerOfMatch.js")
const str=require("./src/server/7-StrikeRatePerSeason.js")
const highestDismiss=require("./src/server/8-HighestDismissal.js")
const superEconomy=require("./src/server/9-SuperOverEconomy.js")

async function main(){
    await csvtojson('deliveries');
    await csvtojson('matches');
    const match=await fs.promises.readFile('./src/data/matches.json');
    const matchJson=await JSON.parse(match,null,2);
    const deliveries=await fs.promises.readFile('./src/data/deliveries.json');
    const deliveriesJson=await JSON.parse(deliveries,null,2);
    //console.log(matchesPerYear(matchJson));

    await fs.promises.writeFile('./src/public/1-MatchesPerYear.json',JSON.stringify(matchesPerYear(matchJson),null,2));
    await fs.promises.writeFile('./src/public/2-MatchesWonPerTeamPerYear.json',JSON.stringify(matchesWonPerTeamPerYear(matchJson),null,2));
    await fs.promises.writeFile('./src/public/3-RunsConcededPerTeam.json',JSON.stringify(runsConcededPerTeam(matchJson,deliveriesJson),null,2));
    await fs.promises.writeFile('./src/public/4-EconomicalBowlers.json',JSON.stringify(economy(matchJson,deliveriesJson),null,2));
    await fs.promises.writeFile('./src/public/5-TossAndMatchWin.json',JSON.stringify(tossAndMatchWin(matchJson),null,2));
    await fs.promises.writeFile('./src/public/6-PlayerOfMatch.json',JSON.stringify(playerOfMatch(matchJson),null,2));
    await fs.promises.writeFile('./src/public/7-StrikeRatePerSeason.json',JSON.stringify(str(matchJson,deliveriesJson),null,2));
    await fs.promises.writeFile('./src/public/8-HighestDismissal.json',JSON.stringify(highestDismiss(deliveriesJson),null,2));
    await fs.promises.writeFile('./src/public/9-SuperEconomy.json',JSON.stringify(superEconomy(matchJson,deliveriesJson),null,2));
}
main();