const fs=require("fs");
const promises=require("fs/promises");

const playerOfMatch=(matches)=>{
    const players={}
    matches.forEach(match=>{
        season=matches.reduce((acc,m)=>{
            if(Number(m['season'])==Number(match['season'])){
                acc[m['player_of_match']]=(acc[m['player_of_match']]||0)+1;
            }
            return acc;
        },[])
        //console.log(season);
        let maxi = -Infinity;
        let string = '';
        for (let key in season) {
            if (maxi < season[key]) {
            maxi = season[key];
            string = key;
            }
            else if(maxi == season[key]){
                string+=' '+ key;
            }
        }
        players[match['season']]=string;
    })
    return players;
}

module.exports=playerOfMatch

// async function main(){
//     const data=await fs.promises.readFile("../data/matches.json");
//     const jsonData=await JSON.parse(data);
//     const ddata=await fs.promises.readFile("../data/deliveries.json");
//     const djsonData=await JSON.parse(ddata);
//     playerOfMatch(jsonData);
// }

// main();