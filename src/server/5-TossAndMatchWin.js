const fs=require("fs");
const promises=require("fs/promises");

const wins=(matches)=>{
    winner={};
    matches.forEach(match=>{
        if(match['winner']==match['toss_winner']){
            winner[match['winner']]=(winner[match['winner']]||0)+1;
        }
    })
    return winner;
}

module.exports=wins;

// async function main(){
//     const data=await fs.promises.readFile("../data/matches.json");
//     const jsonData=await JSON.parse(data);
//     const ddata=await fs.promises.readFile("../data/deliveries.json");
//     const djsonData=await JSON.parse(ddata);
//     wins(jsonData);
// }

// main();