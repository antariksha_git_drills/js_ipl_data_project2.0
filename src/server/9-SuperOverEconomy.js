const fs=require("fs");
const promises=require("fs/promises");

const superEconomy=(match,deliveries)=>{
    totalRuns={};
    totalBalls={};
    deliveries.forEach(deliver=>{
        if(Number(deliver['is_super_over'])==1){
            match.forEach(value=>{
                if(Number(value['id'])==Number(deliver['match_id'])){
                    if(Number(deliver['bye_runs'])==0&&Number(deliver['legbye_runs'])==0){
                        totalRuns[deliver['bowler']]=(totalRuns[deliver['bowler']]||0)
                                                        +Number(deliver['total_runs']);
                    }
                    if(Number(deliver['noball_runs'] == 0 && deliver['wide_runs'] == 0)){
                        totalBalls[deliver['bowler']]=(totalBalls[deliver['bowler']]||0)+1;
                    }
                }
            })
        }
    })
    let economy = {};
    for (let key in totalRuns) {
        economy[key] = (totalRuns[key] * 6) / totalBalls[key];
    }
    const sortedObject = Object.entries(economy).sort((x, y) => x[1]-y[1]);
    const sortedEconomy = sortedObject.reduce(
    (economy, [key, value]) => ({
      ...economy,
      [key]: value,
    }),
    {},
  );
    let topEco = Object.fromEntries(Object.entries(sortedEconomy).slice(0,1));
    return topEco
}

module.exports=superEconomy;

// async function main(){
//     const data=await fs.promises.readFile("../data/matches.json");
//     const jsonData=await JSON.parse(data);
//     const ddata=await fs.promises.readFile("../data/deliveries.json");
//     const djsonData=await JSON.parse(ddata);
//     superEconomy(jsonData,djsonData);
// }

// main();