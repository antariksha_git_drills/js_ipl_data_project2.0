const fs=require("fs");
const promises=require("fs/promises");

const str= (matches, deliveries)=> {
  const result = {};

  deliveries.forEach((delivery) => {
    const matchId = delivery.match_id;
    const batsman = delivery.batsman;
    const runsScored = +delivery.batsman_runs;
    const isWideBall = delivery.wide_runs !== '0';

    const season = matches.find((match) => match.id === matchId)?.season;

    if (!season || isWideBall) {
      return;
    }

    if (!result[season]) {
      result[season] = {};
    }

    if (!result[season][batsman]) {
      result[season][batsman] = {
        runs: 0,
        ballsFaced: 0,
        strikeRate: 0,
      };
    }
    result[season][batsman].runs += runsScored;
    result[season][batsman].ballsFaced += 1;

    const totalBallsFaced = result[season][batsman].ballsFaced;
    const totalRunsScored = result[season][batsman].runs;

    if (totalBallsFaced > 0) {
      result[season][batsman].strikeRate = (
        (totalRunsScored / totalBallsFaced) *
        100
      ).toFixed(2);
    }
  });

  return result;
};

module.exports =str;

// async function main(){
//     const data=await fs.promises.readFile("../data/matches.json");
//     const jsonData=await JSON.parse(data);
//     const ddata=await fs.promises.readFile("../data/deliveries.json");
//     const djsonData=await JSON.parse(ddata);
//     calculateBatsmanStrikeRate(jsonData,djsonData);
// }

// main();