const fs=require("fs");
const promises=require("fs/promises");

const extraRuns=(match,deliveries)=>{
    let extra={};
    match.forEach(i=>{
        if(Number(i['season'])==2016){
            deliveries.forEach(j=>{
                if(Number(i['id'])==Number(j['match_id'])){
                    extra[j['bowling_team']]=(extra[j['bowling_team']]||0)
                                                    +Number(j['extra_runs']);
                }
            })
        }
    })
    return extra;
}

module.exports=extraRuns;

// async function main(){
//     const data=await fs.promises.readFile("../data/matches.json");
//     const jsonData=await JSON.parse(data);
//     const ddata=await fs.promises.readFile("../data/deliveries.json");
//     const djsonData=await JSON.parse(ddata);
//     extraRuns(jsonData,djsonData);
// }

// main();