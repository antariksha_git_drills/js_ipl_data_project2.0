const fs=require("fs");
const promises=require("fs/promises");

const highestDismiss=(deliveries)=>{
    result={};
    deliveries.forEach(deliver=>{
        if(deliver['player_dismissed']!=''){
            result[deliver['player_dismissed']+','+deliver['bowler']]=
                (result[deliver['player_dismissed']+','+deliver['bowler']]||0)+1;
        }
    })
    return maxDismiss(result);
}
function maxDismiss(dismissed) {
  let max = -Infinity;
  let dismiss = {};
  for (let key in dismissed) {
    if (dismissed[key] > max) {
      dismiss = {};
      max = dismissed[key];
      dismiss[key] = dismissed[key];
    } else if (dismissed[key] == max) {
      dismiss[key] = dismissed[key];
    }
  }
  return dismiss;
}

module.exports=highestDismiss;

// async function main(){
//     const data=await fs.promises.readFile("../data/matches.json");
//     const jsonData=await JSON.parse(data);
//     const ddata=await fs.promises.readFile("../data/deliveries.json");
//     const djsonData=await JSON.parse(ddata);
//     highestDismiss(djsonData);
// }

// main();