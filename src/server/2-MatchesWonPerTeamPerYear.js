const fs=require("fs");
const promises=require("fs/promises");

const matchesWonPerTeamPerYear=(match)=>{
    let result={};
    match.forEach(i => {
        let matchesWon=match.reduce((acc,val)=>{
            if(Number(i['season'])===Number(val['season']))
            {
                acc[val['winner']]=(acc[val['winner']]||0);
                acc[val['winner']]+=1;
                return acc;
            }
            else{
                return acc;
            }
        },{});
        result[i['season']]=matchesWon;
    });
    
    //console.log(result);
    return result;
}

module.exports=matchesWonPerTeamPerYear;

// async function main(){
//     const data=await fs.promises.readFile("../data/matches.json");
//     const jsonData=await JSON.parse(data);
//     matchesWonPerTeamPerYear(jsonData);
// }

// main()