const fs=require("fs");
const promises=require("fs/promises");

const matchesPerYear=(matches)=>{
    let result={};
    matches.forEach(i => {
        let noOfmatches=matches.reduce((acc,value)=>{
            if(Number(i['season'])===Number(value['season'])){
                return acc+=1;
            }
            else{
                return acc;
            }
        },0);
        result[i['season']]=noOfmatches;
    });
    return result;
}

module.exports=matchesPerYear;

// async function main(){
//     const data=await fs.promises.readFile("../data/matches.json");
//     const jsonData=await JSON.parse(data);
//     matchesPerYear(jsonData);
// }

// main()